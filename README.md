# Paper references


[1] Biham, E.: A fast new des implementation in software. In: Int. Wksp. on Fast Software Encryption. pp. 260–272 (1997)

[2] Bonneau, J., Mironov, I.: Cache-collision timing attacks against aes. In: CHES. pp. 201–215 (2006)

[3] Chen, S., Liu, F., Mi, Z., Zhang, Y., Lee, R.B., Chen, H., Wang, X.: Leveraging hardware transactional memory for cache side-channel defenses. In: Asian CCS. pp. 601–608 (2018)

[4] Domnitser, L., Jaleel, A., Loew, J., Abu-Ghazaleh, N., Ponomarev, D.: Non-monopolizable caches: Low-complexity mitigation of cache side channel at- tacks. ACM TACO 8(4), 35 (2012)

[5] Goldreich, O., Ostrovsky, R.: Software protection and simulation on oblivious rams. J. ACM 43(3), 431–473 (1996)

[6] Gruss, D., Lettner, J., Schuster, F., Ohrimenko, O., Haller, I., Costa, M.: Strong and efficient cache side-channel protection using hardware transactional memory. In: USENIX Security Symp. (2017)

[7] Gruss, D., Maurice, C., Wagner, K., Mangard, S.: Flush+ flush: a fast and stealthy cache attack. In: Int. Conf. on Detection of Intrusions and Malware, and Vulnerability Assessment. pp. 279–299 (2016)

[8] Gu ̈lmezo ̆glu, B., Inci, M.S., Irazoqui, G., Eisenbarth, T., Sunar, B.: A faster and more realistic flush+ reload attack on aes. In: Int. Wksp. on Constructive Side-Channel Analysis Secure Design. pp. 111–126 (2015)

[9] (Duplicated) Gu ̈lmezo ̆glu, B., I ̇nci, M.S., Irazoqui, G., Eisenbarth, T., Sunar, B.: A faster and more realistic flush+reload attack on aes. In: Constructive Side-Channel Analysis Secure Design. pp. 111–126 (2015)

[10] Irazoqui, G., Eisenbarth, T., Sunar, B.: S $ a: A shared cache attack that works across cores and defies vm sandboxing and its application to aes. In: S&P, IEEE Symp. pp. 591–604 (2015)

[11] Irazoqui, G., Inci, M.S., Eisenbarth, T., Sunar, B.: Wait a minute! a fast, cross-vm attack on aes. In: Int. Wksp. on Recent Advances in Intrusion Detection. pp. 299–319 (2014)

[12] Jiang, Z.H., Fei, Y., Kaeli, D.: A complete key recovery timing attack on a gpu. In: HPCA, IEEE Symp. (2016)

[13] Jiang, Z.H., Fei, Y., Kaeli, D.R.: A novel side-channel timing attack on gpus. In: GLVLSI, ACM Symp. pp. 167–172 (2017)

[14] Jiang, Z.H., Fei, Y.: A novel cache bank timing attack. In: Proc. ICCAD. pp. 139–146 (2017)

[15] Kadam, G., Zhang, D., Jog, A.: Rcoal: mitigating gpu timing attack via subwarp-based randomized coalescing techniques. In: HPCA, IEEE Symp. pp. 156–167 (2018)

[16] Karimi, E., Jiang, Z.H., Fei, Y., Kaeli, D.: A timing side-channel attack on a mobile gpu. In: ICCD, IEEE Conf. pp. 67–74 (2018) Title Suppressed Due to Excessive Length 21

[17] Kim, T., Peinado, M., Mainar-Ruiz, G.: Stealthmem: System-level protection against cache-based side channel attacks in the cloud. In: USENIX Security Symp. pp. 189–204 (2012)

[18] (absent) Kolmogorov, A.: Sulla determinazione empirica di una lgge di distribuzione. Inst. Ital. Attuari, Giorn. 4, 83–91 (1933)

[19] Liu, F., Ge, Q., Yarom, Y., Mckeen, F., Rozas, C., Heiser, G., Lee, R.B.: Catalyst: Defeating last-level cache side channel attacks in cloud computing. In: HPCA, IEEE Symp. pp. 406–418 (2016)

[20] Liu, F., Lee, R.B.: Random fill cache architecture. In: MICRO, IEEE/ACM Int. Symp. pp. 203–215 (2014)

[21] Liu, F., Wu, H., Mai, K., Lee, R.B.: Newcache: Secure cache architecture thwarting cache side-channel attacks. IEEE Micro 36(5), 8–16 (2016)

[22] Liu, F., Yarom, Y., Ge, Q., Heiser, G., Lee, R.B.: Last-level cache side- channel attacks are practical. In: S&P, IEEE Symp. (2015)

[23] Maas, M., Love, E., Stefanov, E., Tiwari, M., Shi, E., Asanovic, K., Kubia- towicz, J., Song, D.: Phantom: Practical oblivious computation in a secure processor. In: CCS, ACM Conf. pp. 311–324 (2013)

[24] Raj, H., Nathuji, R., Singh, A., England, P.: Resource management for isolation enhanced cloud services. In: Proc. of the ACM wksp on Cloud computing security. pp. 77–84 (2009)

[25] Rane, A., Lin, C., Tiwari, M.: Raccoon: Closing digital side-channels through obfuscated execution. In: USENIX Security Symp. pp. 431–446  (2015)

[26] Stefanov, E., Van Dijk, M., Shi, E., Fletcher, C., Ren, L., Yu, X., Devadas,  S.: Path oram: an extremely simple oblivious ram protocol. In: CCS, ACM Conf. pp. 299–310 (2013)

[27] Tromer, E., Osvik, D.A., Shamir, A.: Efficient cache attacks on aes, and  countermeasures. J. of Cryptology 23(1), 37–71 (2010)

[28] Wang, Z., Lee, R.B.: New cache designs for thwarting software cache-based side channel attacks. ACM SIGARCH Computer Architecture News 35(2),  494–505 (2007)

[29] Yarom, Y., Falkner, K.: Flush+ reload: a high resolution, low noise, l3 cache  side-channel attack. In: USENIX Security Symp. pp. 719–732 (2014)

[30] Yarom, Y., Genkin, D., Heninger, N.: Cachebleed: A timing attack on OpenSSL constant time RSA. In: Crypt. Hardware & Embedded Systems (Aug 2016)

[31] Zhou, Z., Reiter, M.K., Zhang, Y.: A software approach to defeating side channels in last-level caches. In: CCS, ACM Conf. pp. 871–882 (2016)